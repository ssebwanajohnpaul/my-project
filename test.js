console.log("hello world !")
//assigning variables
/*cons when we are to re assign the variable another value
let */
const name = "john"
const age = 23;
const y = undefined;
const Iscalm = true;

console.log(typeof y);

//concatenation
console.log('My name is '+name+' and I am ' +age+ ' years')

const s = "Hello dear!";
console.log(s.length);
console.log(s.substring(0, 5));
console.log(s.substring(0, 5).toUpperCase());

//Arrays - are variables that store multiple values
const fruits = ['eggs','goat','sheep']
//inserting to list at the desired position
fruits[3] = 'beans'
//inserting to list at the end of the list
fruits.push("mangoes");
//inserting to the brginning of the list
fruits.unshift("cow peas")
//removing the last item from the list
fruits.pop();
//checking for te presence of the item in the list
console.log(Array.isArray("cow peas"));
//getting index of item in the list
console.log(fruits.indexOf("cow peas"))
console.log(fruits[2]);
console.log(fruits);

//Dictionaries
const bio_data = {
    firstname: 'John paul',
    lastname : "Ssebwana",
    age : 22,
    university: 'makerere',
    colllege : 'cocis',
    hobbies :['volley ball',"music","football"],
    address :{
        street : "william",
        housenumber: 35,
        city : 'entebbe'

    }
}
console.log(bio_data)
console.log(bio_data.firstname, bio_data.lastname)
console.log(bio_data.address.city)
console.log(bio_data.hobbies[2])
