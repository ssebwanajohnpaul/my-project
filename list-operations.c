#include <stdio.h>
#include <stdlib.h>

struct Node
{
        int number;
        struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
        struct Node *head = NULL;
        int choice, data;

        while (1)
        {
                printf("Linked Lists\n");
                printf("1. Print List\n");
                printf("2. Append\n");
                printf("3. Prepend\n");
                printf("4. Delete\n");
                printf("5. Exit\n");
                printf("Enter the choice: ");
                scanf("%d", &choice);

                switch (choice)
                {
                case 1:
                        printList(head);
                        break;
                case 2:
                        printf("Enter value for apppending: ");
                        scanf("%d", &data);
                        append(&head, data);
                        break;
                case 3:
                        printf("Enter a value to be prepended: ");
                        scanf("%d", &data);
                        prepend(&head, data);
                        break;
                case 4:
                        printf("Enter value for deletion from the linked list: ");
                        scanf("%d", &data);
                        deleteByValue(&head, data);
                        break;
                case 5:
                        return 1;
                default:
                        printf("Input is invalid.");
                        break;
                }
        }

        return 0;
}

struct Node *createNode(int num)
{
        struct Node *newNode = malloc(sizeof(struct Node));
        newNode->number = num;
        newNode->next = NULL;
        return newNode;
}

void printList(struct Node *head)
{
        struct Node *temp = head;
        printf("[ ");
        while (temp != NULL)
        {
                printf("%d, ", temp->number);
                temp = temp->next;
        }
        printf(" ]\n");
}

void append(struct Node **head, int num)
{
        struct Node *newNode = createNode(num);
        if (*head == NULL)
        {
                *head = newNode;
                return;
        }
        struct Node *temp = *head;
        while (temp->next != NULL)
        {
                temp = temp->next;
        }
        temp->next = newNode;
}

void prepend(struct Node **head, int num)
{
        struct Node *newNode = createNode(num);
        newNode->next = *head;
        *head = newNode;
}

void deleteByKey(struct Node **head, int key)
{
        struct Node *temp = *head, *prev = NULL;
        if (temp != NULL && temp->number == key)
        {
                *head = temp->next;
                free(temp);
                return;
        }
        while (temp != NULL && temp->number != key)
        {
                prev = temp;
                temp = temp->next;
        }
        if (temp == NULL)
        {
                return;
        }
        prev->next = temp->next;
        free(temp);
}

void deleteByValue(struct Node **head, int value)
{
        struct Node *temp = *head, *prev = NULL;
        if (temp != NULL && temp->number == value)
        {
                *head = temp->next;
                free(temp);
                return;
        }
        while (temp != NULL && temp->number != value)
        {
                prev = temp;
                temp = temp->next;
        }
        if (temp == NULL)
        {
                return;
        }
        prev->next = temp->next;
        free(temp);
}

void insertAfterKey(struct Node **head, int key, int value)
{
        struct Node *temp = *head;
        while (temp != NULL && temp->number != key)
        {
                temp = temp->next;
        }
        if (temp == NULL)
        {
                return;
        }
        struct Node *newNode = createNode(value);
        newNode->next = temp->next;
        temp->next = newNode;
}

void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
        struct Node *temp = *head;
        while (temp != NULL && temp->number != searchValue)
        {
                temp = temp->next;
        }
        if (temp == NULL)
        {
                return;
        }
        struct Node *newNode = createNode(newValue);
        newNode->next = temp->next;
        temp->next = newNode;
}
