#include <stdio.h>
#include <stdlib.h>
//NAME : SSEBWANA JOHN PAUL
//STUDENT NO.: 2300717415
//REG. NO.: 23/U/17415/PS

//solving quadratic equation ax^2 + bx + c
// by using bull dozer
// x = (-b + sqrt(discriminant))/2*a or (-b - sqrt(discriminant))/2*a
int main()
{
    double a, b, c, discriminant,root, root1, root2;

 printf("Enter coefficients a: ");
 scanf("%lf" , &a);
 printf("Enter coefficient b: ");
 scanf("%lf",&b);
 printf("Enter coefficient c: " );
 scanf("%lf",&c);
 if (a==0){
    printf("Error: a cannot be zero");
 }
 else{
    discriminant = pow(b,2) -4 * a * c;
    if (discriminant>0){
        printf("Two distinct roots\n");
        root1 = (-b+sqrt(discriminant))/2*a;
        root2 = (-b-sqrt(discriminant))/2*a;
        printf("root1 = %lf\n",root1);
        printf("root2 = %lf\n",root2);
    }else if (discriminant == 0){
        printf("one real root\n");
        root = -b/2*a;
        printf("root = %lf",root);
    }else{
        printf("complex root");

    }
 }
    return 0;
}
