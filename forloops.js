//for loops
for(let i=0; i<10; i++){
    console.log(`the value for loop is ${i}`)
}
//while loop
i = 0
while(i<10){
    console.log(`the number for while loop: ${i}`);
    i++;
}
const details = [
    {
        name : 'ssebwana',
        address : 'masaka',
        college : 'cocis'
    },
    {
        name : 'katongole',
        address : 'mbarara',
        college : 'cedat'
    },
    {
        name : 'brian',
        address : 'mityana',
        college : 'chuss'
    }
]
for(let i = 0; i < details.length; i++){
    console.log(i)
}
const person = [
    {
        name : 'ssebwana',
        address : 'masaka',
        college : 'cocis'
    },
    {
        name : 'katongole',
        address : 'mbarara',
        college : 'cedat'
    },
    {
        name : 'brian',
        address : 'mityana',
        college : 'chuss'
    }
]
for(let i = 0; i < person.length; i++){
    console.log(person[i].address)
}
for(let infor of person){
    console.log(infor.name);
}
//forEach which loops through an array
//map which creates a new array from the array
//filter which creates a new array based on the condition

// these high order loops take a function and (the desired name for the variable)
person.forEach(function(data){
    console.log(data.address);
}

);
//map since it returns an array, we assign the variable
const new_array = person.map(function(data){
    return data.name;
});
console.log(new_array);

//filter
const target_name = person.filter(function(select){
    return select.name === 'ssebwana'
}
);
console.log(target_name);

//when to print the specific item in the selected piece of array
const specific_item = person.filter(function(select){
    return select.name === 'ssebwana'
}
).map(function(specific){
    return specific.address;
}

)
console.log(specific_item);
